import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarModule } from './navbar/navbar.module';
import { NavbarConfig } from './navbar/interfaces/navbar-config';

export const navbarConfig: NavbarConfig = {
  title: 'My list App',
  menu: [
    {
      label: 'List',
      routerLink: ['list']
    },
    {
      label: 'List Completed',
      routerLink: ['list', 'completed']
    }
  ]
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NavbarModule.forRoot(navbarConfig),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
