import { InjectionToken } from '@angular/core';

export const NavbarConfigToken = new InjectionToken('NavbarConfig');
