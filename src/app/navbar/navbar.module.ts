import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NavbarConfigToken } from './constants/config-token.constant';
import { NavbarConfig } from './interfaces/navbar-config';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [NavbarComponent],
  exports: [NavbarComponent],
  imports: [
    CommonModule,
    RouterModule,
  ]
})
export class NavbarModule {

  static forRoot(config: NavbarConfig): ModuleWithProviders {
    return {
      ngModule: NavbarModule,
      providers: [{
        provide: NavbarConfigToken, useValue: config
      }]
    };
  }

}
