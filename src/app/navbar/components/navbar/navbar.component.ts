import { Component, Inject, OnInit } from '@angular/core';
import { NavbarConfigToken } from '../../constants/config-token.constant';
import { NavbarConfig } from '../../interfaces/navbar-config';

/**
 * Navbar Component
 * Used for create navbar for app use config NavbarConfigToken
 * @example
 * <app-navbar></app-navbar>
 */
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  menuCollapse = true;

  constructor(
    @Inject(NavbarConfigToken) private config: NavbarConfig,
  ) {
  }

}
