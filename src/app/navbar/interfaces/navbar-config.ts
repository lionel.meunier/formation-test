export interface NavbarConfig {
  title: string;
  menu: NavbarMenuItem[];
}

export interface NavbarMenuItem {
  label: string;
  routerLink: string[];
}
