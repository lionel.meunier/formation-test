import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path : '',
    redirectTo : 'list',
    pathMatch : 'full'
  },
  {
    path: 'list',
    loadChildren : './ma-list/ma-list.module#MaListModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing : true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
