import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaListComponent } from './components/ma-list/ma-list.component';
import { MaListCompletedComponent } from './components/ma-list-completed/ma-list-completed.component';
import { MaListRoutingModule } from './ma-list-routing.module';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CompletedDirective } from './directives/completed.directive';
import { MyTitlePipe } from './pipes/my-title.pipe';
import { ListItemComponent } from './components/list-item/list-item.component';

@NgModule({
  declarations: [MaListComponent, MaListCompletedComponent, ItemDetailComponent, CompletedDirective, MyTitlePipe, ListItemComponent],
  exports: [MaListComponent, MaListCompletedComponent],
  imports: [
    CommonModule,
    MaListRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers : [
  ]
})
export class MaListModule { }
