import { Directive, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[appCompleted]'
})
export class CompletedDirective {

  @Output() completeChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @HostBinding('class.fas')
  @HostBinding('class.fa-check')
  @Input('appCompleted') completed: boolean;

  @HostBinding('class.far')
  @HostBinding('class.fa-square')
  get uncheck() {
    return !this.completed;
  }

  @HostListener('click', ['$event'])
  clickTo(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    this.completeChange.emit(true);
  }


  constructor() {
  }

}
