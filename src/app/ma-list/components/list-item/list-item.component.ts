import { Component, Input, OnInit } from '@angular/core';
import { ListItem, MaListService } from '../../services/ma-list.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {
  /**
   * Item for display
   */
  @Input() item: ListItem;
  @Input() prefixeTitle: string;
  @Input() suffixeTitle: string;

  constructor(private service: MaListService) {
  }

  /**
   * Complete one item in list use service
   */
  onCompleted() {
    this.service.toggleCompletedItem(this.item);
  }
}
