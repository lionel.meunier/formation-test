import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaListComponent } from './ma-list.component';

describe('MaListComponent', () => {
  let component: MaListComponent;
  let fixture: ComponentFixture<MaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
