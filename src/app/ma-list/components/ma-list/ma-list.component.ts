import { ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { ListItem, MaListService } from '../../services/ma-list.service';

/**
 * MaList Component
 * Used for create route "list/"
 * @example
 * <app-ma-list></app-ma-list>
 */
@Component({
  selector: 'app-ma-list',
  templateUrl: './ma-list.component.html',
  styleUrls: ['./ma-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MaListComponent implements OnInit {
  /**
   * Items getting in MaListService for display
   */
  items$: Observable<ListItem[]>;

  constructor(private service: MaListService) {
  }

  /**
   * Init component get items in service
   */
  ngOnInit() {
    this.items$ = this.service.getAll();
  }

  /**
   * Complete one item in list use service
   * @param element Item in list
   */
  onCompleted(element: ListItem) {
    this.service.toggleCompletedItem(element);
  }


}
