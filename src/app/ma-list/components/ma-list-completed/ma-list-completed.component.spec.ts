import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaListCompletedComponent } from './ma-list-completed.component';

describe('MaListCompletedComponent', () => {
  let component: MaListCompletedComponent;
  let fixture: ComponentFixture<MaListCompletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaListCompletedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaListCompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
