import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MaListComponent } from '../ma-list/ma-list.component';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ListItem } from '../../services/ma-list.service';

@Component({
  selector: 'app-ma-list-completed',
  templateUrl: './ma-list-completed.component.html',
  styleUrls: ['./ma-list-completed.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MaListCompletedComponent extends MaListComponent implements OnInit {
  itemsFiltered$: Observable<ListItem[]>;

  ngOnInit() {
    super.ngOnInit();
    this.itemsFiltered$ = this.items$.pipe(
      map((items) => {
        return items.filter(el => el.completed);
      })
    );

  }

}
