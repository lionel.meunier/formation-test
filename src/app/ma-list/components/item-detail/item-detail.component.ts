import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, timer } from 'rxjs';
import { ListItem } from '../../services/ma-list.service';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemDetailComponent implements OnInit {

  item$: Observable<ListItem>;
  itemId$: Observable<string>;
  @ViewChild('myForm', {static: false}) form: FormGroup;
  myFormSubmitted = false;
  myReactiveForm: FormGroup;
  myReactiveFormSubmitted = false;

  constructor(
    private httpClient: HttpClient,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private chRef: ChangeDetectorRef
    ) {
  }

  ngOnInit() {
    this.itemId$ = this.activatedRoute.params.pipe(
      map(params => params.id),
      filter(id => !!id),
    );
    this.item$ = this.activatedRoute.params
      .pipe(
        map(params => params.id),
        filter(id => !!id),
        switchMap(id => {
          return this.httpClient.get(`https://jsonplaceholder.typicode.com/todos/${id}`).pipe(
          );
        }),
        map((result: ListItem) => {
          this.constructForm(result);
          return result;
        })
      );
  }

  onSubmit(form: NgForm) {
    if (form.valid && form.touched) {
      this.myFormSubmitted = true;
      timer(2000)
        .pipe(
        )
        .subscribe(() => {
          this.myFormSubmitted = false;
          this.chRef.detectChanges();
        });
    }
  }

  onSubmitReactive() {
    if (this.myReactiveForm.valid && this.myReactiveForm.touched) {
      this.myReactiveFormSubmitted = true;
      timer(2000)
        .pipe(
        )
        .subscribe(() => {
          this.myReactiveFormSubmitted = false;
          this.chRef.detectChanges();
        });
    }

  }

  private constructForm(element) {
    this.myReactiveForm = this.formBuilder.group({
      title: new FormControl(element.title, [Validators.required, Validators.minLength(3)]),
      completed: new FormControl(element.completed, []),
    });
  }

}
