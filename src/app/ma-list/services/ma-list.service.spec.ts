import { TestBed } from '@angular/core/testing';

import { MaListService } from './ma-list.service';

describe('MaListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaListService = TestBed.get(MaListService);
    expect(service).toBeTruthy();
  });
});
