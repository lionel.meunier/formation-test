import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface ListState {
  items: ListItem[];
  isInit: boolean;
}

export interface ListItem {
  id: number;
  title: string;
  completed: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class MaListService {

  subject$: BehaviorSubject<ListState>;
  data$: Observable<ListItem[]>;
  state: ListState = {
    items: [],
    isInit: false
  };

  constructor(private httpClient: HttpClient) {
    this.subject$ = new BehaviorSubject(this.state);

    this.data$ = this.subject$.pipe(
      map(state => {
        return state.items;
      }));
  }

  getAll(): Observable<ListItem[]> {
    if (!this.state.isInit) {
      this.state.isInit = true;
      this.httpClient.get('https://jsonplaceholder.typicode.com/todos').subscribe((value: any[]) => {
        this.state.items = value;
        this.subject$.next(this.state);
      });
    }
    return this.data$;
  }

  toggleCompletedItem(item) {
    const itemFinded = this.state.items.find((el) => el.id === item.id);
    if (itemFinded) {
      itemFinded.completed = !itemFinded.completed;
      this.subject$.next(this.state);
    } else {
      console.error('This Item is not in cache', item);
    }
  }
}
