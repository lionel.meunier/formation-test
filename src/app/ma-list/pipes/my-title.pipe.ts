import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myTitle'
})
export class MyTitlePipe implements PipeTransform {

  transform(value: string, ...args: [string?, string?]): string {
    const prefixe = args && args[0] ? args[0] : '===';
    const suffixe = args && args[1] ? args[1] : prefixe;

    return `${prefixe}${value.toUpperCase()}${suffixe}`;
  }

}
