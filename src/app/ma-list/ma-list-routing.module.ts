import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MaListComponent } from './components/ma-list/ma-list.component';
import { MaListCompletedComponent } from './components/ma-list-completed/ma-list-completed.component';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';


const routes: Routes = [

  {
    path : 'completed',
    component : MaListCompletedComponent,
    children: [
      {
        path: ':id',
        component: ItemDetailComponent
      }
    ]
  },
  {
    path: '',
    component: MaListComponent,
    children: [
      {
        path: ':id',
        component: ItemDetailComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaListRoutingModule { }
